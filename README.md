**Pre requisitos**

* Tener instalado docker
* Tener instalado maven
* Tener instalado Java 1.8

**Pasos**

* crear un directorio en el tmp para la metadata

````
mkdir /tmp/solr
chmod 777 /tmp/solr
````

* crear un contenedor con solr

`docker run --name my_solr -d -p 8983:8983 -v /tmp/solr:/var/solr/data/ -t solr`


* crear un core en el solr

`docker exec -it --user=solr my_solr bin/solr create_core -c edimca-products1`

* crear un contenerdor con postgresql

`docker run --name postgres11 -p 5432:5432 -e POSTGRES_PASSWORD=welcome1 -d postgres`

* crear la base de datos solr en postgrsql

````
docker exec -it postgres11 bin/bash
su postgres
psql
create database solr;
\q
exit
exit
````

* Correr el demo

`./mvnw spring-boot:run`


Gracias!!!


Comentarios a @_ferpaz
