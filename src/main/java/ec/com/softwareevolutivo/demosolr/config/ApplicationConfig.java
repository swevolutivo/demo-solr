package ec.com.softwareevolutivo.demosolr.config;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.solr.core.SolrOperations;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;
import org.springframework.data.solr.server.support.HttpSolrClientFactory;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableSolrRepositories
@EnableScheduling
public class ApplicationConfig {

    @Bean
    public SolrClient solrClient() {
        HttpSolrClient solrClient = new HttpSolrClient.Builder("http://localhost:8983/solr").build();
        HttpSolrClientFactory factory = new HttpSolrClientFactory(solrClient);
        return factory.getSolrClient();
    }

    @Bean
    public SolrOperations solrTemplate() {
        return new SolrTemplate(solrClient());
    }
}
