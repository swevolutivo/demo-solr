package ec.com.softwareevolutivo.demosolr.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
public class Product implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    @NotNull
    private String name;
    private String category;
    private String usage;
    private String relatedProductsId;
    @Transient
    private List<String> _relatedProductsId = new ArrayList<>();
    private String family;
    private Double price;
    private String keywords;
    @Transient
    private List<String> _keywords = new ArrayList<>();;

    public Product() {
    }

    public Product(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public String getRelatedProductsId() {
        return relatedProductsId;
    }

    public void setRelatedProductsId(String relatedProductsId) {
        this.relatedProductsId = relatedProductsId;
    }

    public List<String> get_relatedProductsId() {
        return relatedProductsId != null ? Arrays.asList(relatedProductsId.split(",")) : _relatedProductsId;
    }

    public void set_relatedProductsId(List<String> _relatedProductsId) {
        this.relatedProductsId  = String.join(",", _relatedProductsId);
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public List<String> get_keywords() {
        return keywords != null ? Arrays.asList(keywords.split(",")) : _keywords;
    }

    public void set_keywords(List<String> _keywords) {
        this.keywords = String.join(",", _keywords);
    }
}
