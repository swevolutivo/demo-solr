package ec.com.softwareevolutivo.demosolr.controller;

import ec.com.softwareevolutivo.demosolr.model.Product;
import ec.com.softwareevolutivo.demosolr.service.ProductService;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/products")
public class ProductController {
    private static final String ENTITY_NAME = "product";

    @Autowired
    ProductService productService;

    @GetMapping("/{id}")
    public Product get(@PathVariable(name = "id") String id) throws IOException, SolrServerException {
        Product product = productService.findById(id);
        return product;
    }

    @GetMapping("/query/{searchTerm}")
    public List<Product> query(@PathVariable(name = "searchTerm") String searchTerm) throws IOException, SolrServerException {
        List<Product> products = productService.findByTerm(searchTerm);
        return products;
    }

    @PostMapping
    public ResponseEntity<Product> create(@Valid @RequestBody Product product) throws IOException, SolrServerException {
        product = productService.save(product);
        return ResponseEntity.ok().body(product);
    }
}
