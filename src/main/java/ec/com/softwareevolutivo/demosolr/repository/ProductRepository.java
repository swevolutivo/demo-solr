package ec.com.softwareevolutivo.demosolr.repository;

import ec.com.softwareevolutivo.demosolr.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
}
