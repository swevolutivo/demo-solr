package ec.com.softwareevolutivo.demosolr.service;

import ec.com.softwareevolutivo.demosolr.model.Product;
import ec.com.softwareevolutivo.demosolr.repository.ProductRepository;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class ProductService {
    public static final String EDIMCA_PRODUCTS = "edimca-products1";

    @Autowired
    SolrClient solrClient;

    @Autowired
    ProductRepository productRepository;

    public Product save(Product product) throws IOException, SolrServerException {
        final SolrInputDocument doc = new SolrInputDocument();
        doc.addField("id", product.getId());
        doc.addField("name", product.getName());
        final UpdateResponse updateResponse = solrClient.add(EDIMCA_PRODUCTS, doc);
        solrClient.commit(EDIMCA_PRODUCTS);
        return product;
    }

    public Product findById(String id) throws IOException, SolrServerException {
        SolrDocument document =  solrClient.getById(EDIMCA_PRODUCTS, id);
        if (document == null) {
            return null;
        }
        Product product = translateSolrDocumentToProduct(document);
        return product;
    }

    private Product translateSolrDocumentToProduct(SolrDocument document) {
        Long _id = Long.valueOf((String) document.getFieldValue("id"));
        List<String> _name = (List<String>) document.getFieldValue("name");
        Product product = new Product(_id, _name.get(0));
        List<String> _category = (List<String>) document.getFieldValue("category");
        if (_category != null && _category.size() > 0) {
            product.setCategory(_category.get(0));
        }
        List<String> _usage = (List<String>) document.getFieldValue("usage");
        if (_usage != null && _usage.size() > 0) {
            product.setUsage(_usage.get(0));
        }
        Collection<Object> _relatedProductsId = document.getFieldValues("relatedProductsId");
        if (_relatedProductsId != null) {
            for (Object _relatedProductId : _relatedProductsId) {
                product.get_relatedProductsId().add(((Long) _relatedProductId).toString());
            }
        }
        List<String> _family = (List<String>) document.getFieldValue("family");
        if (_family != null && _family.size() > 0) {
            product.setFamily(_family.get(0));
        }
        List<Double> _price = (List<Double>) document.getFieldValue("price");
        if (_price != null && _price.size() > 0) {
            product.setPrice(_price.get(0));
        }
        Collection<Object> _keywords = document.getFieldValues("keywords");
        if (_keywords != null) {
            for (Object keyword : _keywords) {
                product.get_keywords().add((String) keyword);
            }
        }
        return product;
    }

    public List<Product> findByTerm(String searchTerm) throws IOException, SolrServerException {
        List<Product> products = new ArrayList<>();
        StringBuilder _searchTerm = new StringBuilder();
        for (String field : new String[]{"name", "category", "usage", "family", "keywords"}) {
            _searchTerm.append(field).append(":").append(searchTerm).append(" ");
        }
        SolrQuery query = new SolrQuery();
        query.setQuery(_searchTerm.toString());
        query.setStart(0);
        query.setRows(10);
        QueryResponse response = solrClient.query(EDIMCA_PRODUCTS, query);
        SolrDocumentList results = response.getResults();
        if (results.size() == 0) {
            return null;
        }
        for (SolrDocument document : results) {
            Product product = translateSolrDocumentToProduct(document);
            products.add(product);
        }

        return products;
    }

    @Scheduled(fixedRate = 20000)
    public void indexData() throws IOException, SolrServerException {
        for (Product product : productRepository.findAll()) {
            final SolrInputDocument doc = new SolrInputDocument();
            doc.addField("id", product.getId());
            doc.addField("name", product.getName());
            doc.addField("category", product.getCategory());
            doc.addField("usage", product.getUsage());
            for (String relatedProductId :  product.get_relatedProductsId()) {
                doc.addField("relatedProductsId", relatedProductId);
            }
            doc.addField("family", product.getFamily());
            doc.addField("price", product.getPrice());
            for (String keyword :  product.get_keywords()) {
                doc.addField("keywords", keyword);
            }
            final UpdateResponse updateResponse = solrClient.add(EDIMCA_PRODUCTS, doc);
        }
        solrClient.commit(EDIMCA_PRODUCTS);
    }

}
